#!/bin/bash
#just a small bash script to calculate BFs for all SNPs from SNPFILE
#please see the Bayenv2 manual for details about usage

SNPFILE=$1
ENVFILE=$2
MATFILE=$3
POPNUM=$4
ITNUM=$5
ENVNUM=$6


split -d -a 10 -l 2 $SNPFILE snp_batch

for f in snp_batch*
do
./bayenv2 -i $f -e $ENVFILE -m $MATFILE -k $ITNUM -r $RANDOM -p $POPNUM -n $ENVNUM -t
done

rm -f snp_batch*
